matlabFaceRecognitionRealTime
=============================
There are two modues to this software. One is training a new face , and the other is capturing a face and checking the database for the face. 


TO train the software with a new face, open Matlab and run the function training(n). Here, n is an integer argument to the function training. 
For every new face you train the software with, u have to increment the value of n. 

To run the software, use the function official(). This takes no arguments.

NOTE: This program requires matlab version r2015a to run.
