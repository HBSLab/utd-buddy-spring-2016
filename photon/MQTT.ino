// This #include statement was automatically added by the Particle IDE.
#include "MQTT/MQTT.h"

// Define MQTT credentials
#define MQTT_USER "photon"
#define MQTT_PASSWORD "Buddy@UTD"
#define MQTT_CLIENTID "ParticlePhoton1" //can be anything else
#define MQTT_SERVER "m10.cloudmqtt.com"
#define MQTT_SUBSCRIBE_TOPIC "/buddy/angles"
#define MQTT_PORT 14753

// Define Servo pins
#define LEFT_HAND_SHOULDER_SERVO_PORT D0
#define LEFT_HAND_ELBOW_SERVO_PORT D1
#define LEFT_HAND_WRIST_SERVO_PORT D3

#define RIGHT_HAND_SHOULDER_SERVO_PORT A4
#define RIGHT_HAND_ELBOW_SERVO_PORT A5
#define RIGHT_HAND_WRIST_SERVO_PORT WKP

#define NECK_TILT_SERVO_PORT RX

#define LEFT_HAND_SHOULDER_MIN 0
#define LEFT_HAND_SHOULDER_MAX 180
#define RIGHT_HAND_SHOULDER_MIN 0
#define RIGHT_HAND_SHOULDER_MAX 180

#define LEFT_HAND_ELBOW_BEND_MIN 18
#define LEFT_HAND_ELBOW_BEND_MAX 90
#define RIGHT_HAND_ELBOW_BEND_MIN 18
#define RIGHT_HAND_ELBOW_BEND_MAX 90

#define NECK_TILT_MIN 0
#define NECK_TILT_MAX 90

// Declare all the functions.
void attachServos();
void mqtt_connect();
void setInitialServoPositions();
int parseCommand(String command);

int setLeftHandShoulder(String posValue);
int setLeftHandElbow(String posValue);
int setLeftHandWrist(String posValue);

int setRightHandWrist(String posValue);
int setNeckTilt(String posValue);

int getPosition(Servo servo);
void moveServo(Servo servo);
int setPosition(Servo servo, String posValue, int oldValue);

void callback(char* topic, byte* payload, unsigned int length);

MQTT client(MQTT_SERVER, MQTT_PORT, callback);

// A max of 8 servos can be connected
Servo leftHandShoulderServo, leftHandElbowServo, leftHandWristServo;
Servo rightHandShoulderServo, rightHandElbowServo, rightHandWristServo;
Servo neckTiltServo;

// Store the servo positions.
int leftHandShoulderServoPos = 0;
int leftHandElbowServoPos = 0;
int leftHandWristServoPos = 0;
int rightHandShoulderServoPos = 0;
int rightHandElbowServoPos = 0;
int rightHandWristServoPos = 0;
int neckTiltServoPos = 0;

void setup()
{
  Serial.begin(9600);

  // Attach all the servos to  the respective pins.
  attachServos();

  // setInitialServoPositions();

  // Function & Variable names should be only 12 chars long.
  // Also only a maximum of 4 functions can be registered.
  // So, lets have a single function that delegates the command
  // to other functions.
  //Spark.function("parseCommand", parseCommand);

  //Spark.variable("getLHSPos", &leftHandShoulderServoPos, INT);
  //Spark.variable("getLHEPos", &leftHandElbowServoPos, INT);
  //Spark.variable("getLHWPos", &leftHandWristServoPos, INT);

  mqtt_connect();
}

void loop() {
    // Loop if connected to the server.
    // Otherwise, try reconnecting after a delay of 1s.
    if (client.isConnected()) {
        client.loop();
    } else {
        client.disconnect();
        // Try after 1s so that we don't load the MQTT server
        delay(1000);
        mqtt_connect();
    }
}

// Attaches all the servo motors to the respective pins
void attachServos() {
  leftHandShoulderServo.attach(LEFT_HAND_SHOULDER_SERVO_PORT);
  leftHandElbowServo.attach(LEFT_HAND_ELBOW_SERVO_PORT);
  leftHandWristServo.attach(LEFT_HAND_WRIST_SERVO_PORT);

  rightHandShoulderServo.attach(RIGHT_HAND_SHOULDER_SERVO_PORT);
  rightHandElbowServo.attach(RIGHT_HAND_ELBOW_SERVO_PORT);
  rightHandWristServo.attach(RIGHT_HAND_WRIST_SERVO_PORT);

  neckTiltServo.attach(NECK_TILT_SERVO_PORT);
}

// Connects to the MQTT server and subscribes to the angles topic
void mqtt_connect() {
    // connect to the server
    client.connect(MQTT_CLIENTID, MQTT_USER, MQTT_PASSWORD);

    // subscribe to MQTT in QOS Level 1 so that we don't miss any data
    // TODO: Check if QOS level 1 slows down the speed of Buddy and if so,
    // use QOS0 where the ACK message isn't sent.
    if (client.isConnected()) {
        Serial.println("connected");
        client.subscribe(MQTT_SUBSCRIBE_TOPIC, MQTT::QOS1);
    }
    else {
        Serial.println("not connected");
    }
}

// recieve message
void callback(char* topic, byte* payload, unsigned int length) {
    char p[length + 1];
    memcpy(p, payload, length);
    p[length] = NULL;
    String command(p);

    // Let's print what we got in the console.
    // Serial.println(command);
    parseCommand(command);
}

// Gathers all the servo motor positions and stores in the respective
// variables.
void setInitialServoPositions() {
    leftHandShoulderServoPos = getPosition(leftHandShoulderServo);
    leftHandElbowServoPos = getPosition(leftHandElbowServo);
    leftHandWristServoPos = getPosition(leftHandWristServo);

    rightHandWristServoPos = getPosition(rightHandWristServo);

    neckTiltServoPos = getPosition(neckTiltServo);
}

// Parses the given command whose structure is as below
// and writes positions to the respective servos.
// Format: 'lsw:45,rsw:56,lss:45,rss:135'
int parseCommand(String command) {
  // Get rid of leading/trailing spaces.
  command.trim();

  Serial.println("command: " + command);
  // Exit if command is empty.
  if(command.length() == 0)
    return -1;

  // First split by ',' and in each of the comma separated
  // strings search for ':' to find key value pairs.
  int commaIndex = command.indexOf(",");
  int colonIndex = command.indexOf(":", 0);
  int beginIndex = 0;

  // Loop until either commaIndex or colonIndex isn't null
  // to ensure that the last 'key:value' pair gets parsed.
  while (commaIndex != -1 || colonIndex != -1) {
    // Anything before the colon is a command and after it is a parameter.
    String servoCmd = command.substring(beginIndex, colonIndex);
    String servoPos;
    // If it's the last key value pair, parameter should be till the end
    if(commaIndex == -1)
      servoPos = command.substring(colonIndex + 1, command.length());
    else
      servoPos = command.substring(colonIndex + 1, commaIndex);

    servoCmd.trim();
    servoPos.trim();

    // Now let's set the respective servo positions if their position haven't
    // changed since the last write.
    if(servoCmd == "lss") {
      setLeftHandShoulder(servoPos);
    }
    else if(servoCmd == "rss") {
      setRightHandShoulder(servoPos);
    }
    else if(servoCmd == "lsw") {
      setLeftHandElbow(servoPos);
    }
    else if(servoCmd == "rsw") {
      setRightHandElbow(servoPos);
    }
    else if(servoCmd == "lew") {
      setLeftHandWrist(servoPos);
    }
    else if(servoCmd == "rew") {
      setRightHandWrist(servoPos);
    }
    else if(servoCmd == "nt") {
      setNeckTilt(servoPos);
    }

    // Break once the last key:value pair is parsed.
    if(commaIndex == -1)
      break;
  	beginIndex = commaIndex + 1;
  	commaIndex = command.indexOf(",", beginIndex);
    colonIndex = command.indexOf(":", beginIndex);
  }

  return 1;
}

// Sets the left hand shoulder position and returns the position set.
int setLeftHandShoulder(String posValue) {
  leftHandShoulderServoPos = setPosition(leftHandShoulderServo, posValue, leftHandShoulderServoPos);

  return leftHandShoulderServoPos;
}

// Sets the right hand shoulder position and returns the position set.
int setRightHandShoulder(String posValue) {
  rightHandShoulderServoPos = setPosition(rightHandShoulderServo, posValue, rightHandShoulderServoPos);

  return rightHandShoulderServoPos;
}

// Sets the left hand elbow position and returns the position set.
int setLeftHandElbow(String posValue) {
  leftHandElbowServoPos = setPosition(leftHandElbowServo, posValue, leftHandElbowServoPos);

  return leftHandElbowServoPos;
}

// Sets the right hand elbow position and returns the position set.
int setRightHandElbow(String posValue) {
  rightHandElbowServoPos = setPosition(rightHandElbowServo, posValue, rightHandElbowServoPos);

  return rightHandElbowServoPos;
}

// Sets the left hand wrist position and returns the position set.
int setLeftHandWrist(String posValue) {
  leftHandWristServoPos = setPosition(leftHandWristServo, posValue, leftHandWristServoPos);

  return leftHandWristServoPos;
}

// Sets the right hand wrist position and returns the position set.
int setRightHandWrist(String posValue) {
  rightHandWristServoPos = setPosition(rightHandWristServo, posValue, rightHandWristServoPos);

  return rightHandWristServoPos;
}

// Sets the neck tilt position and returns the position set.
int setNeckTilt(String posValue) {
  neckTiltServoPos = setPosition(neckTiltServo, posValue, neckTiltServoPos);

  return neckTiltServoPos;
}

// Returns the current angle of the given servo
int getPosition(Servo servo) {
    int angle = servo.read();

    return angle;
}

// Sets the given angle to the given servo iff the new angle
// isn't the old angle.
int setPosition(Servo servo, String posValue, int oldValue) {
    int pos = posValue.toInt();
    // Write only when the position has to be changed.
    if(pos != oldValue)
      servo.write(pos);

    return pos;
}

void moveServo(Servo servo) {
  for(int pos = 0; pos < 180; pos += 5)  // goes from 0 degrees to 180 degrees
  {                                  // in steps of 1 degree
    servo.write(pos);              // tell servo to go to position in variable 'pos'
    delay(100);                       // waits 15ms for the servo to reach the position
  }
  for(int pos = 180; pos>=1; pos-=5)     // goes from 180 degrees to 0 degrees
  {
    servo.write(pos);              // tell servo to go to position in variable 'pos'
    delay(100);                       // waits 15ms for the servo to reach the position
  }
}